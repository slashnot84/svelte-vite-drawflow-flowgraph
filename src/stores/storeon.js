import { createStoreon } from 'storeon'

let graphEditor = store => {
    store.on('@init', () => ({ graphEditor: {} }))

    store.on('SET_GRAPH_EDITOR', (state, graphEditor) => {
        return { ...state, graphEditor }
    })
}

export const storeon = createStoreon([graphEditor])