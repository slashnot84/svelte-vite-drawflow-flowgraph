
import { writable } from 'svelte/store'
import { produce } from 'immer'

const { set, update, subscribe } = writable({ editor: null, nodes: [] })
export default {
    subscribe,
    set,
    update,

    setState: (values) => {
        update(state => ({ ...state, ...values }))
    },

    setEditor: (editor) => {
        update(state => ({ ...state, editor }))
    },

    addNode: (nodeName = '') => update(state => {
        return produce(state, draft => {
            draft.nodes.push({ name: nodeName.toLowerCase().replace(/[^A-Z0-9]+/ig, "_") })
            return draft
        })
    })
}