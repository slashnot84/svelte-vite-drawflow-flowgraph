import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import yaml from '@rollup/plugin-yaml';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [yaml(), svelte()],
  resolve: {
    alias: {
      'src': path.resolve('src/'),
      'lib': path.resolve('src/lib/'),
      'components': path.resolve('src/components/'),
      'assets': path.resolve('src/assets/'),
      'hooks': path.resolve('src/hooks/'),
      'services': path.resolve('src/services/'),
      'utils': path.resolve('src/utils/'),
      'stores': path.resolve('src/stores/'),
    }
  }
})
